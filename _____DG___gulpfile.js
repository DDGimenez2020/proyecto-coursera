'use strict';

var gulp = require('gulp');
var	sass = require('gulp-sass');
/*Agregamos el browser sync*/
var	browserSync= require('browser-sync');

/*Configuramos la primer tarea*/

gulp.task('sass', function(){
	 gulp.src('./css/*.scss')
		.pipe(sass().on('error', sass.logError))
		//destino
		.pipe(gulp.dest('./css'));
});	

/*Agregamos una tarea para el monitoreo del watch para los archivos scss*/

gulp.task('sass:watch', function(){
	gulp.watch('./css/*.scss', ['sass']);
});	

/*Agregamos una tarea para el buscar los cambios que hay en el proyecto*/
gulp.task('browser-sync', function(){
	var files = ['./*.html','./css/*.css','./img/*.{png, jpg, gif}','js/.*js'];
	browserSync.init(files,{
		server:{
			baseDir: './'
		}
	});
});

/*Generamos la tarea defaul para que con un unico paso tenemos el sitio levantado*/
gulp.task('default',['browser-sync'],function(){
	gulp.start('sass:watch');
});